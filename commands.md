* `git init`
* `git status`
* `git add`
* `git diff`
* `git commit`
* `git log`
* `git checkout`
* `git merge`


## `git init`

Creates a repository.

Creates a subdirectory named `.git` that contains all necessary data. All the version control data is in `.git` directory.

Run `tree -a` to see what happens when you run `git init`.

    $ ls -la
    total 12
    drwxrwxr-x 3 narendra narendra 4096 Feb 13 02:12 .
    drwxrwxr-x 3 narendra narendra 4096 Feb 13 01:12 ..
    drwxrwxr-x 7 narendra narendra 4096 Feb 13 02:12 .git

    $ tree -a
      .
      └── .git
          ├── branches
          ├── config
          ├── description
          ├── HEAD
          ├── hooks
          │   ├── applypatch-msg.sample
          │   ├── commit-msg.sample
          │   ├── post-update.sample
          │   ├── pre-applypatch.sample
          │   ├── pre-commit.sample
          │   ├── prepare-commit-msg.sample
          │   ├── pre-push.sample
          │   ├── pre-rebase.sample
          │   └── update.sample
          ├── info
          │   └── exclude
          ├── objects
          │   ├── info
          │   └── pack
          └── refs
              ├── heads
              └── tags



### Info about files:

* **description**
* **config** : project specific configuration options
* **info** : patterns to be ignored as defined in .gitignore file
* **hooks**: client side or server side hooks.
* **HEAD**: points to currently checked out branch
* **objects** : database of almost everything git stores
* **refs** : pointers to commit objects in the data
* **index** : staging information is stored in index file

### More analysis

    $ touch a.txt

    $ git add .

    $ tree -a
    .
    ├── a.txt
    └── .git
        ├── branches
        ├── config
        ├── description
        ├── HEAD
        ├── hooks
        │   ├── applypatch-msg.sample
        │   ├── commit-msg.sample
        │   ├── post-update.sample
        │   ├── pre-applypatch.sample
        │   ├── pre-commit.sample
        │   ├── prepare-commit-msg.sample
        │   ├── pre-push.sample
        │   ├── pre-rebase.sample
        │   └── update.sample
        ├── index
        ├── info
        │   └── exclude
        ├── objects
        │   ├── e6
        │   │   └── 9de29bb2d1d6434b8b29ae775ad8c2e48c5391
        │   ├── info
        │   └── pack
        └── refs
            ├── heads
            └── tags

Add a commit.

    $ git commit -m "first commit"
    [master (root-commit) 28d1d5a] first commit
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 a.txt

    $ tree -a
    .
    ├── a.txt
    └── .git
        ├── branches
        ├── COMMIT_EDITMSG
        ├── config
        ├── description
        ├── HEAD
        ├── hooks
        │   ├── applypatch-msg.sample
        │   ├── commit-msg.sample
        │   ├── post-update.sample
        │   ├── pre-applypatch.sample
        │   ├── pre-commit.sample
        │   ├── prepare-commit-msg.sample
        │   ├── pre-push.sample
        │   ├── pre-rebase.sample
        │   └── update.sample
        ├── index
        ├── info
        │   └── exclude
        ├── logs
        │   ├── HEAD
        │   └── refs
        │       └── heads
        │           └── master
        ├── objects
        │   ├── 28
        │   │   └── d1d5accbfb30c465c0b7be6b7c72db25d05760
        │   ├── 65
        │   │   └── a457425a679cbe9adf0d2741785d3ceabb44a7
        │   ├── e6
        │   │   └── 9de29bb2d1d6434b8b29ae775ad8c2e48c5391
        │   ├── info
        │   └── pack
        └── refs
            ├── heads
            │   └── master
            └── tags

In objects sub-directory, we have all these objects. This is the data we've committed named as checksum. If you unzip that file, what you'll get is the raw data.

So what are these 3 files:
* file contents
* directory listing
* commit metadatas

Commit object points to the tree and tree points to the blob. So Git finds commit, and then traverse the tree to find all the data.

How do you find the commit? Branches.

## `git commit`

    $ git commit -m "Add new file"
    [master 5bc6142] Add new file
    1 file changed, 0 insertions(+), 0 deletions(-)
    create mode 100644 b.txt

5bc6142 : version number (first 7 chars of 40 chars long SHA)


## `git clone`

Clones a repository with **all branches**, but checks out active branch as starting point.

## `git status`

Show the **working tree** status.

Displays paths:
* that have differences between the **index file** and the current **HEAD commit**.
* that have differences between the **working tree** and the **index file**.
* in the **working tree** that are not tracked by Git.

**It's healthy to run `git status` often. Sometimes things change and you don't notice it.**

## :mag_right: `HEAD`

HEAD points to last known state of your working directory.

## `git branch`

List all braches. Create new branch.

`master` is default branch. It's not mandatory to have a `master` branch. It's just a pointer.

`git checkout dev-fix-issue1` creates a new branches pointing to `HEAD`.

`git branch` lists all the branches.

    $ git branch
    dev-fix-issue1
    dev-one
    * master

    $ find .git/refs
    .git/refs
    .git/refs/tags
    .git/refs/heads
    .git/refs/heads/master
    .git/refs/heads/dev-fix-issue1
    .git/refs/heads/dev-one

    $ cat .git/refs/heads/master
    5bc6142793fc5f5715da132111bdd2f9f4cbc7ea

Folowing command moves head to `dev-fix-issue1`

    $ git checkout dev-fix-issue1

Explore: `git branch -r`

`git checkout -b iss2 master` is same as `git checkout master && git checkout -b iss2`.

## `git checkout`

Switch between branches.


## `git merge`

Branching structure of Git makes merging a every easy task.

It is called in the context you are in. Not the one you're merging from.

Default merge is **fast forward merge**. Doesn't create a new snapshot. One branch isn't reachable from another in fast forward merge.

**non-fast forward merge**: Git traves the path backwards and finds the first common first merge-base and does a 3 way merge.

**Branches are lightweight, movable pointers to a commit.**

Merge conflicts occur when both sides of the merge have changed the same file. Automatic merging fails.

A `git add` is required after manual merging. Run `git commit` after that.

In there are no merge conflicts, git will automatically stge the changes and commit them. You can override that behavior.

Why branching is cool?
* try out a new idea
* isolate work units
* long running topics

`git branch -d branch-name` deletes the branch. Small `d` is safety feature. It won't delete work which isn't reachable.

## `git push`

Git does not have a concept of central server.

It has concept of **remote**. Remote is an alias for a URL pointing to a git repository.

If you do `git clone` you'll get a remote called `origin`. Actually you'll have a remote for every branch.   
If you do `git init` you won't get any remote. You'll have to create one.

`origin` is the default name Git gives to remote. You can rename it or delete it and create a new remote, and Git will work just fine.

`git push` doesn't do any merge. It simply takes the new commits' sequence and adds it to the remote commit sequence.


## `git fetch`


## `git pull`

`git fetch` followed by `git merge`.


## fork and pull requests


## `git log`

Walks the history from HEAD.

To get history from a different starting point: `git log branch-name`

Lots of formatting options.

`git log --oneline`
`git log --oneline --graph`
`git log --oneline --graph --all --decorate`

### log subsets

`git log branchA ^branchB`

What work is in branchA that I haven't merged in master yet?

`git log branchA ^master`
