# Git

Git is an **opensource**, **distributed** **version control system** designed for *speed* and *efficiency*.

Any other distributed version control system other than Git? Mercurial?

Centralized systems? Subversion.
The data is on a central server. And a developer is more like a client for that server.

## Fully distributed

**Everything is local (almost).**

Peer to peer push and pull.  
Git only needs an another database to sync with. That's it. It doesn't care about anything else.

### Implications of local behavior

Everything is fast. Diff, check history, how each file looked at any point of time, who changed which file and when, create, use and merge multiple branches, get previous version of file.

Every clone is a backup.

You're on a plane 35k feet up in the air or middle of Sahara desert, network or no network, you can do just about everything (except sync).

You can have multiple clones of a repository on same file system. You can push and pull to local clones and the mechanism behind that push and pull is exactly same as pushing to remote sever sitting in another continent (except the network part, obviously).

So, **remote** is simply another version of database.

### :bulb:
What database does Git uses?


## Immutable: Integrity

**Never removes data (almost)**

`rebase` rewrites history? Sounds familiar?

Git never deletes the data. It simply moves it somewhere else. So do not rewrite history, you write a new history and move the pointer to it. Does not mean that previous history is deleted forever. It simply has moved to a newer place. The data is in there somewhere.

## Snapshots

Concept of snapshot is central to Git.

**What Git doesn't do**: store file deltas.

**What git does**: Snapshots! Track contents of file.

Git add doesn't add file names. It adds content of file to next commit.

Git takes the content of file, checksums it and stores it into database as key-value pair.

When you say Git commit, what it does is this: It records a manifest, a tree object. Manifest contains content checksum and file names.

So since everything is checksummed, you can't change the data out of the Git randomly. Cryptographically secured.

Git does't store any explicit rename, or move information. It checks what files disappeared in previous commit and what new files appeared and whose content checksums are same.

## States

* committed
* staged
* modified

.git -> working ares -> staging area
