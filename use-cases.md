# use-cases

* How to undo the most recent commits?
* How to delete a Git branch both locally and remotely?
* How to modify existing, un-pushed commits?
* How to undo `git add` before `git commit`?
* How to rename local branch?
* How to checkout to a previous version?
* How to un-track an already tracked file? 3-4 cases. :mag:
* How to force `git pull` to override local changes?
* How to move most recent commits to a new branch?
* How to clone all remote branches, not only the default one? :laughing:
* How to push a new local branch to a remote Git repository and track it too?
* How to revert a specific file to a specific revision using Git?
* How to push a local branch to remote with a different branch name at remote?
