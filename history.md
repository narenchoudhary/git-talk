# Short History

* Linux kernel was maintained throught patches, archieves
* In 2002, they started using BitKeeper.
* In 2005, Bitkeeper revoked free-of-charge status for Linux kernel.
* Linus Tarvolds developed Git.
* Rest is history.

## Goals

* Speed
* Simple design
* Non linear development
* Distributed
* Suitable for large projects
