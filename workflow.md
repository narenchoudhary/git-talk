### Basic workflow

* Edit files
* Stage the changes: `git add`. Allows patching also (`-p` or `--patch` flag). A snapshot is taken at this point.
* Review the changes: `git status`, `git diff`, `git show :` (shows indexed files' contents)
* Commit the changes: `git commit` does not look at your working directory at all. It simply commits the staged changes.
