
* ~`git commit -am "commit message"` is same as `git add . && git commit -m "commit message"`.~ Not really. `git add .` adds all changes including new files (which are not being tracked previously), while `git commit -am` adds only the changes in the files being tracked previously.
* Do not do `git rebase` until you know what you are doing. It's a powerful command, but things can go wrong. Moreever, HSBC GitHub does not allow commits to master after rebase. :disappointed: :disappointed: HSBC GitHub doesn't allow force push also. (`git push -f`)
* I prefer `git fetch` followed by `git merge` manually rather than `git pull`.
* `git merge` is called in the context you are in. Not the one you're merging from. Do incremental merges. Git allows that. 50 merges everyday is better than 500 merges at day 10. Keep your branch up to date. Easier said than done. But still try to follow it.
* `git config --global color.ui true`
* Use cooler shells: **fish** and **zsh** are awesome. (Not possible in HSBC, especially with Windows)
